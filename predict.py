from unet import Unet
from PIL import Image
import os
import cv2
import sys
import tensorflow as tf

from keras import backend as K

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


def resource_path(relative_path):
    if getattr(sys, 'frozen', False):
        base_path = sys._MEIPASS
    else:
        base_path = os.path.abspath(".")
    return os.path.join(base_path, relative_path)


def divide_img(img_path, img_name, size, f_path):
    unet = Unet()
    imgg = os.path.join(img_path, img_name)
    img = cv2.imread(imgg)
    h = img.shape[0]
    w = img.shape[1]
    top = (size - h % size) // 2
    bottom = (size - h % size) - top
    left = (size - w % size) // 2
    right = (size - w % size) - left
    # print("h:{} w:{} top:{} bottom:{} left:{} right:{}".format(h,w,top,bottom,left,right))
    img = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=[0, 0, 0])
    h1 = img.shape[0]
    w1 = img.shape[1]
    # print("h1:{} w1:{}".format(h1,w1))
    dis_h = size
    dis_w = size
    n = h1 // dis_h
    m = w1 // dis_w
    num = 0
    name = img_name[:-4]
    form = img_name[-3:]
    save_path = resource_path(os.path.join("res", "imgin")) + '\\'
    os.makedirs(save_path, exist_ok=True)
    # save_path = './imgin/'
    for i in range(n):
        for j in range(m):
            num += 1
            sub = img[dis_h * i:dis_h * (i + 1), dis_w * j:dis_w * (j + 1), :]
            cv2.imwrite(save_path + '{}{:0>3}.{}'.format(name, num, form), sub)
    imgs = os.listdir(save_path)
    for jpg in imgs:
        img = Image.open(save_path + jpg)
        seg_img1 = unet.detect_image(img)

        c_path = os.path.join(save_path, jpg)
        os.remove(c_path)
        name = jpg[:-3]
        seg_img1.save(save_path + name + 'png')
    IMAGES_FORMAT = ['.JPG', '.png', '.jpg']
    IMAGE_SIZE = size
    IMAGE_ROW = n
    IMAGE_COLUMN = m

    # 获取图片集地址下的所有图片名称
    image_names = [fname for fname in os.listdir(save_path) for item in IMAGES_FORMAT if
                   os.path.splitext(fname)[1] == item]

    # 简单的对于参数的设定和实际图片集的大小进行数量判断

    if len(image_names) != IMAGE_ROW * IMAGE_COLUMN:
        raise ValueError("合成图片的参数和要求的数量不能匹配！")
    to_image = Image.new('RGB', (IMAGE_COLUMN * IMAGE_SIZE, IMAGE_ROW * IMAGE_SIZE))  # 创建一个新图
    # 循环遍历，把每张图片按顺序粘贴到对应位置上
    for y in range(1, IMAGE_ROW + 1):
        for x in range(1, IMAGE_COLUMN + 1):
            from_image = Image.open(save_path + image_names[IMAGE_COLUMN * (y - 1) + x - 1]).resize(
                (IMAGE_SIZE, IMAGE_SIZE), Image.ANTIALIAS)
            to_image.paste(from_image, ((x - 1) * IMAGE_SIZE, (y - 1) * IMAGE_SIZE))
    to_image.save(save_path + img_name[:-3] + 'png')

    to_image = cv2.imread(save_path + img_name[:-3] + 'png')
    """
    x0 = (w1 - w) // 2
    x1 = w1 - x0
    y0 = (h1 - h) // 2
    y1 = h1 - y0
    """
    x0 = left
    x1 = w1 - right
    y0 = top
    y1 = h1 - bottom
    # print("x0:{} x1:{} y0:{} y1:{}".format(x0,x1,y0,y1))

    cropped = to_image[y0:y1, x0:x1]
    cv2.imwrite(os.path.join(f_path, img_name[:-3] + 'png'), cropped)
    ls = os.listdir(save_path)
    for i in ls:
        c_path = os.path.join(save_path, i)
        os.remove(c_path)


if __name__ == '__main__':
    img_path = './img/'
    savepath = './imgout/'
    os.makedirs(savepath, exist_ok=True)
    size = 512
    img_list = os.listdir(img_path)
    for name in img_list:
        divide_img(img_path, name, size, savepath)
        K.clear_session()
        tf.reset_default_graph()
