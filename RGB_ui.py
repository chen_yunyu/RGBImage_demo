# -*- coding: utf-8 -*-
import sys
import os
import cv2
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QImage
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox, QFileDialog
from mainWin import Ui_MainWindow
from functools import partial
from predict import resource_path, divide_img
import tensorflow as tf
import numpy as np

from keras import backend as K


class MyMainForm(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MyMainForm, self).__init__(parent)
        self.setupUi(self)

        # Crop
        self.Btn_crop_src.clicked.connect(partial(self.openFile, self.Pth_crop_src))
        self.Btn_crop_dst.clicked.connect(partial(self.openFile, self.Pth_crop_dst))
        self.Btn_crop_ok.clicked.connect(self.crop)

        # Segmentation
        self.Btn_seg_src.clicked.connect(partial(self.openFile, self.Pth_seg_src))
        self.Btn_seg_dst.clicked.connect(partial(self.openFile, self.Pth_seg_dst))
        self.Btn_seg_ok.clicked.connect(self.segmentation)

        # getMask
        self.Btn_mask_src.clicked.connect(partial(self.openFile, self.Pth_mask_src))
        self.Btn_mask_dst.clicked.connect(partial(self.openFile, self.Pth_mask_dst))
        self.Btn_mask_ok.clicked.connect(self.getMask)

    def openFile(self, lineEdit):
        get_directory_path = QFileDialog.getExistingDirectory(self,
                                                              "选取指定文件夹",
                                                              ".")
        lineEdit.setText(str(get_directory_path))

    def crop(self):
        basePth = self.Pth_crop_src.text()
        savePth = self.Pth_crop_dst.text()
        os.makedirs(savePth, exist_ok=True)
        isOdd = False
        if self.station_1.isChecked():
            cropDict_Odd = {'1.png': [1500, 150, 3300, 1750],
                            '2.png': [1500, 150, 3300, 1750],
                            '3.png': [400, 150, 2200, 1750],
                            '4.png': [400, 150, 2200, 1750],
                            '5.png': [550, 150, 2350, 1750],
                            '6.png': [550, 150, 2350, 1750],
                            '7.png': [500, 150, 2300, 1750],
                            '8.png': [500, 150, 2300, 1750],
                            '9.png': [550, 150, 2350, 1750],
                            '10.png': [550, 150, 2350, 1750],
                            '11.png': [600, 150, 2400, 1750],
                            '12.png': [600, 150, 2400, 1750],
                            '13.png': [450, 150, 2250, 1750],
                            '14.png': [450, 150, 2250, 1750]}

            cropDict_Even = {'1.png': [450, 150, 2250, 1750],
                             '2.png': [450, 150, 2250, 1750],
                             '3.png': [600, 150, 2400, 1750],
                             '4.png': [600, 150, 2400, 1750],
                             '5.png': [550, 150, 2350, 1750],
                             '6.png': [550, 150, 2350, 1750],
                             '7.png': [500, 150, 2300, 1750],
                             '8.png': [500, 150, 2300, 1750],
                             '9.png': [550, 150, 2350, 1750],
                             '10.png': [550, 150, 2350, 1750],
                             '11.png': [400, 150, 2200, 1750],
                             '12.png': [400, 150, 2200, 1750],
                             '13.png': [1500, 150, 3300, 1750],
                             '14.png': [1500, 150, 3300, 1750]}

        elif self.station_2.isChecked():
            cropDict_Odd = {'1.png': [1250, 550, 3050, 2150],
                            '2.png': [1250, 550, 3050, 2150],
                            '3.png': [200, 550, 2000, 2150],
                            '4.png': [200, 550, 2000, 2150],
                            '5.png': [250, 550, 2050, 2150],
                            '6.png': [250, 550, 2050, 2150],
                            '7.png': [400, 550, 2200, 2150],
                            '8.png': [400, 550, 2200, 2150],
                            '9.png': [250, 550, 2050, 2150],
                            '10.png': [250, 550, 2050, 2150],
                            '11.png': [400, 550, 2200, 2150],
                            '12.png': [400, 550, 2200, 2150],
                            '13.png': [350, 550, 2150, 2150],
                            '14.png': [350, 550, 2150, 2150]}

            cropDict_Even = {'1.png': [350, 550, 2150, 2150],
                             '2.png': [350, 550, 2150, 2150],
                             '3.png': [400, 550, 2200, 2150],
                             '4.png': [400, 550, 2200, 2150],
                             '5.png': [250, 550, 2050, 2150],
                             '6.png': [250, 550, 2050, 2150],
                             '7.png': [400, 550, 2200, 2150],
                             '8.png': [400, 550, 2200, 2150],
                             '9.png': [250, 550, 2050, 2150],
                             '10.png': [250, 550, 2050, 2150],
                             '11.png': [200, 550, 2000, 2150],
                             '12.png': [200, 550, 2000, 2150],
                             '13.png': [1250, 550, 3050, 2150],
                             '14.png': [1250, 550, 3050, 2150]}
        else:
            sys.exit(1)

        for line in os.listdir(basePth):

            isOdd = not isOdd
            linePth = os.path.join(basePth, line)

            if 14 != len(os.listdir(linePth)):
                self.Output_crop.append("No enough images: %s !" % linePth)
                continue
            else:
                self.Output_crop.append("Processing: %s" % linePth)

            self.cursot = self.Output_crop.textCursor()
            self.Output_crop.moveCursor(self.cursot.End)
            QtWidgets.QApplication.processEvents()

            for img in os.listdir(linePth):

                if isOdd:
                    left, top, right, bottom = cropDict_Odd[img]
                else:
                    left, top, right, bottom = cropDict_Even[img]

                src = cv2.imread(os.path.join(linePth, img))
                cropImg = src[top:bottom, left:right, :]
                # cv2.namedWindow("Crop", cv2.WINDOW_NORMAL)
                # cv2.imshow("Crop", cropImg)
                # cv2.waitKey(0)
                savePth_img = os.path.join(savePth, line, img)
                os.makedirs(os.path.join(savePth, line), exist_ok=True)
                cv2.imwrite(savePth_img, cropImg)

    def segmentation(self):
        basePth = self.Pth_seg_src.text()
        savePth = self.Pth_seg_dst.text()
        os.makedirs(savePth, exist_ok=True)
        for line in os.listdir(basePth):
            linePth = os.path.join(basePth, line)
            savePth_line = os.path.join(savePth, line)
            os.makedirs(savePth_line, exist_ok=True)

            size = 512
            img_list = os.listdir(linePth)
            for name in img_list:
                self.Output_seg.append("Processing: %s %s" % (linePth, name))
                self.cursot = self.Output_seg.textCursor()
                self.Output_seg.moveCursor(self.cursot.End)
                QtWidgets.QApplication.processEvents()
                divide_img(linePth, name, size, savePth_line)
                K.clear_session()
                tf.reset_default_graph()

    def getMask(self):
        if self.RBtn_L.isChecked():
            self.Output_mask.append("To getting Leaf Mask...")
            flag = 'L'
        elif self.RBtn_P.isChecked():
            self.Output_mask.append("To getting Panicle Mask...")
            flag = 'P'
        else:
            self.Output_mask.append("To getting Leaf and Panicle Mask...")
            flag = 'LP'

        basePth = self.Pth_mask_src.text()
        savePth = self.Pth_mask_dst.text()
        os.makedirs(savePth, exist_ok=True)
        for line in os.listdir(basePth):
            linePth = os.path.join(basePth, line)
            savePth_line = os.path.join(savePth, line)
            os.makedirs(savePth_line)
            self.Output_mask.append("Processing: %s" % linePth)
            self.cursot = self.Output_mask.textCursor()
            self.Output_mask.moveCursor(self.cursot.End)
            QtWidgets.QApplication.processEvents()

            for img in os.listdir(linePth):
                src = cv2.imread(os.path.join(linePth, img), 0)
                if 'L' == flag:
                    dst = np.where(src == 88, 255, 0)
                if 'P' == flag:
                    dst = np.where(src == 255, 255, 0)
                if 'LP' == flag:
                    dst = np.where(src == 88, 255, src)

                cv2.imwrite(os.path.join(savePth_line, img), dst)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    myWin = MyMainForm()
    myWin.show()
    sys.exit(app.exec_())
